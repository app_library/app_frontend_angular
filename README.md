# Application Library (Client-Side)

## Description

The Clientside of the application was developed on a node server using the Angular framework (Why? because it's my favorite clientside framework). I used docker to create a multi-container application to simulate distant servers, so i will explain how to set it up later on.

## Folder Structure

```
- app (All our structure will be in the app folder)
    -   features  (Where we save our main features)
        -   application-system  (View & Some basic logic of our application-system)
    -   pages  (The different pages of our website)
    -   services  (Our main logic and http requests to the server)
        -   application-service  (The service of our "applications")
```
---

## Installation

Docker is an open source containerization platform that will enable us to pack our application into containers (Literally independant OS).

Docker installation Guides (If you already have docker installed, you can skip this step):
- [Docker Desktop Download](https://www.docker.com/products/docker-desktop/)
- [Install WSL 2](https://docs.microsoft.com/fr-fr/windows/wsl/install)
- [Get started with Docker](https://docs.microsoft.com/en-us/windows/wsl/tutorials/wsl-containers)
- [Container with VSCODE](https://code.visualstudio.com/docs/remote/containers-tutorial)

Installing our container as node environment
```docker
# Command to paste into your CLI after you installed the docker environment
docker run -dti --name APP_LIBRARY_CS -p 4200:4200 node
```

When you are connected to your container with vscode, you will need to pull the git repository.

```sh
# Preparation of our folders & git install
mkdir //home/my-app && cd //home/my-app
apt update && apt install git -y
```

```sh
# Pull our project and save the remote as origin
git init
git remote add origin https://gitlab.com/app_library/app_frontend_angular.git
git pull origin master
npm install -g @angular/cli
npm install
```

> Note: If you change any port that i used in the docker runs, dont forget to change it in code aswell or the application will not work.
---

## Running the Application


```sh
# Make sure you are on the right directory ( cd //home/my-app )
ng serve -o
```

---

## Disclaimer

> Every time that i mention "application" i'm not referring to our "clientside application", i'm referring to the Feature/Entity Application.

---

Enjoy, thanks ;)