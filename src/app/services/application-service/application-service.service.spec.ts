/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApplicationServiceService } from './application-service.service';

describe('Service: ApplicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationServiceService]
    });
  });

  it('should ...', inject([ApplicationServiceService], (service: ApplicationServiceService) => {
    expect(service).toBeTruthy();
  }));
});
