import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Application_Impl } from 'src/app/features/application-system/application/application.component';
import * as socketIo from 'socket.io-client';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  /**
   * The property will hold every application in the clientside
   */
  private subject: BehaviorSubject<any> = new BehaviorSubject([]);

  private backendURL = 'http://localhost:8080';


  constructor(
    private http: HttpClient,
  ) {
    this.http.get(this.backendURL + "/application")
      .subscribe(applications => {
        this.subject.next(applications);
      })
  }

  /**
   * The method uploadApplication will return an http request post as an observable.
   * 
   * @param form 
   * @returns Observable<any>
   */
  public uploadApplication(form: FormData): Observable<any> {
    return this.http.post<any>(this.backendURL + "/application", form);
  }

  /**
   * It will returns as an observable every single application.
   * 
   * @returns Observable<Application_Impl>
   */
  public getAll(): Observable<Application_Impl[]> {
    return this.subject.asObservable();
  }

  /**
   * Update a single application (Serverside Only).
   * 
   * @returns void
   */
  public update(form: any, application: Application_Impl): Observable<any> {
    return this.http.patch<any>(this.backendURL + "/application", {
      form: form,
      application: application
    });
  }

  /**
   * It will insert and update our current subject with the new data (Clientside Only).
   * 
   * @returns void
   */
  public insertIntoSubject(application: Application_Impl): void {
    let applications = this.subject.getValue();
    applications.push(application);
    this.subject.next(applications);
  }

  /**
   * It will update a specific application in our subject (Clientside Only).
   * 
   * @returns void
   */
  public updateApplicationSubject(newData: any, incomingApplication: Application_Impl): void {
    let applications = this.subject.getValue();
    let application = applications.find((app: Application_Impl) => app.id === incomingApplication.id)

    application.name = newData.name;
    application.description = newData.description;

    this.subject.next(applications);
  }
}
