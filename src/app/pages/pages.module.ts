import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ApplicationSystemModule } from '../features/application-system/application-system.module';
import { HomepageComponent } from './homepage/homepage.component';
import { LibraryComponent } from './library/library.component';



@NgModule({
  declarations: [
    HomepageComponent,
    LibraryComponent
  ],
  imports: [
    CommonModule,
    ApplicationSystemModule
  ],
  exports: [
    HomepageComponent,
    LibraryComponent
  ]
})
export class PagesModule { }
