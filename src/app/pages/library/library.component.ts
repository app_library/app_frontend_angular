import { Component, OnInit } from '@angular/core';
import { Application_Impl } from 'src/app/features/application-system/application/application.component';
import { ApplicationService } from 'src/app/services/application-service/application-service.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {

  private applications!: Application_Impl[];

  /**
   * Instance generation of ApplicationService
   * @param applicationService 
   */
   constructor(
    public applicationService: ApplicationService
  ) { }

  ngOnInit() {
    this.applicationService.getAll()
      .subscribe(data => this.applications = data)
  }

  /**
   * Get all applications in our view component
   * 
   * @returns Application_Impl[]
   */
  public getApplications(): Application_Impl[] {
    return this.applications;
  }


}
