import { Component, Input, OnInit } from '@angular/core';
import { ApplicationService } from 'src/app/services/application-service/application-service.service';
import { Application_Impl } from '../application/application.component';

@Component({
  selector: 'app-update-application',
  templateUrl: './update-application.component.html',
  styleUrls: ['./update-application.component.scss']
})
export class UpdateApplicationComponent implements OnInit {

  // A specific instance of an application
  @Input() application!: Application_Impl;

  // It creates an instance of our addApplicationForm
  public updateApplicationForm = {
    name: '',
    description: ''
  }

  /**
   * Instance generation of ApplicationService
   * @param applicationService 
   */
   constructor(
    public applicationService: ApplicationService
  ) { }

  ngOnInit() {
    this.updateApplicationForm = {
      name: this.application.name,
      description: this.application.description!
    }
  }

  /**
   * After a FormData instance is generated we will call our Dependancy Injection ("ApplicationService") that will give us the possibility of calling the update method. We will receive in a Callback the response from the server.
   */
   public submitForm(): void {
    if(this.handlerErrorForm()){
      const FORM_DATA = {
        name: this.updateApplicationForm.name,
        description: this.updateApplicationForm.description
      }

      this.applicationService.update(FORM_DATA, this.application)
        .subscribe(response => {
          if(response.length === 1 && response[0] === 1)
            this.applicationService.updateApplicationSubject(FORM_DATA, this.application);
        });
    }
    return;
  }

  /**
   * In the case that we have an error in our form, the handler will return false, otherwise, it will return true.
   * 
   * @returns boolean
   */
   public handlerErrorForm(): boolean {
    if(!this.nameFormVerification())
      return false;
    
    return true;
  }

  /**
   * nameFormVerification is a method that will verify if the name is not an empty string or it respects the max and min length, if everything is respected, it will return true, otherwise it will return false;
   * 
   * @returns boolean
   */
  public nameFormVerification(): boolean {
    if(this.updateApplicationForm.name === '')
      return false;

    if(this.updateApplicationForm.name.length > 50 || this.updateApplicationForm.name.length < 5)
      return false;

    return true;
  }

}
