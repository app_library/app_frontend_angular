import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';
import { AddApplicationComponent } from './add-application/add-application.component';
import { ApplicationComponent } from './application/application.component';
import { UpdateApplicationComponent } from './update-application/update-application.component';



@NgModule({
  declarations: [
    AddApplicationComponent,
    ApplicationComponent,
    UpdateApplicationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    AddApplicationComponent,
    ApplicationComponent,
    UpdateApplicationComponent
  ]
})
export class ApplicationSystemModule { }
