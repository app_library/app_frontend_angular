import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {

  @Input() application!: Application_Impl;

  constructor() { }

  ngOnInit() {
  }

}

export interface Application_Impl {
  id: number;
  reference: string;
  name: string;
  description?: string;
  createdAt: string;
  updatedAt: string;
}
