import { Component, OnInit } from '@angular/core';
import { ApplicationService } from 'src/app/services/application-service/application-service.service';

@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.scss']
})
export class AddApplicationComponent implements OnInit {

  // It creates an instance of our addApplicationForm
  public addApplicationForm = {
    name: '',
    description: '',
    file: ''
  }

  /**
   * Instance generation of ApplicationService
   * @param applicationService 
   */
  constructor(
    public applicationService: ApplicationService
  ) { }

  ngOnInit() { }

  /**
   * After a FormData instance is generated we will call our Dependancy Injection ("ApplicationService") that will give us the possibility of calling the uploadApllication method. We will receive in a Callback the response from the server.
   */
  public submitForm(): void {
    if(this.handlerErrorForm()){
      const FORM_DATA = new FormData();

      FORM_DATA.append("name", this.addApplicationForm.name);
      FORM_DATA.append("file", this.addApplicationForm.file);
      FORM_DATA.append("description", this.addApplicationForm.description);

      this.applicationService.uploadApplication(FORM_DATA)
        .subscribe(response => {
          if(response)
            this.applicationService.insertIntoSubject(response);
        });
    }
    return;
  }

  /**
   * In the case that we have an error in our form, the handler will return false, otherwise, it will return true.
   * 
   * @returns boolean
   */
  public handlerErrorForm(): boolean {
    if(!this.nameFormVerification())
      return false;

    if(!this.fileFormVerification())
      return false;
    
    return true;
  }

  /**
   * nameFormVerification is a method that will verify if the name is not an empty string and it respects the max and min length, if everything is respected, it will return true, otherwise it will return false;
   * 
   * @returns boolean
   */
  public nameFormVerification(): boolean {
    if(this.addApplicationForm.name === '')
      return false;

    if(this.addApplicationForm.name.length > 50 || this.addApplicationForm.name.length < 5)
      return false;

    return true;
  }

  /**
   * fileFormVerification is a method that will verify if we submitted a file, if we did in fact submit a file, it will return true, otherwise it will return false.
   * 
   * @returns 
   */
   public fileFormVerification(): boolean {
    if(this.addApplicationForm.file === '')
      return false;

    return true;
  }

  /**
   * Sets the property this.addApplicationForm.file as the new file that we submitted.
   * @param event 
   */
  onFileSelected(event: any): void {
    this.addApplicationForm.file = event.target.files[0];
  }
}
