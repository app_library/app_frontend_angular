import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { LibraryComponent } from './pages/library/library.component';

const routes: Routes = [
  {
    path: 'homepage',
    component: HomepageComponent
  },
  {
    path: 'library',
    component: LibraryComponent
  },
  {
    // It will redirect to the Homepage if we try an empty string.
    path: '',
    component: HomepageComponent
  },
  {
    // It will redirect to the Homepage if we try routes that dont actually exist.
    path: '**',
    component: HomepageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
